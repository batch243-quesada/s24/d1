// console.log('Hello, World');

// ES6 Updates

//// exponent operator ////
// before es6
// let x = 6 ** 9

// ES6
// let x = Math.pow(6, 9);

//// Template Literals ////
// `${variable}`
// allow us to write strings with embedded JavaScript
// `${variable1 * variable2}`


//// Array Destructuring ////
// allows us to unpack elements in arrays intro distinct variables
// allows us to name array elements with variables instead of using index numbers
// let/const [varA, varB, ...] = arrayName;

// before array destructuring
const fullName = ['Juan', 'Dela', 'Cruz'];

// Array destructuring
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(fullName);
console.log(`Hello ${firstName} ${middleName} ${lastName}.`)


//// Object Destructuring ////
// allows us to unpack properties of objects into distinct variables
// let/const {propertyA, propertyB, propertyC, ...} = objectName;
const person = {
	givenName: 'Jane',
	maidenName: 'Delos',
	familyName: 'Santos'
};

const {
	maidenName,
	familyName,
	givenName
} = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);

function getFullName({givenName, maidenName, familyName}) {
	console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFullName(person);


//// Arrow Functions ////
// compact alternate syntax to traditional functions

//// Arrow Functions with Loops ////
const students = ['John', 'Jane', 'Judy'];

students.forEach(student => {
	console.log(`${student} is a student!`);
});

//// Implicit Return Statement ////
// there are instances when you can omit return statement. this works because even without return statement, JavaScript implicitly adds it for the result of the function
const subtract = (x, y) => x - y;

console.log(subtract(100, 31));


//// Default Function Argument Value ////
// provides a default argument value if none is provided when the function is invoked
const greet = (name = 'User') => {
	return `Good morning, ${name}!`;
}

console.log(greet())

// const functionName = (x, y) => {
// 	return x + y;
// }

// functionName(1, 2);


//// Class-based Object Blueprints ////
// allows us to create/instantiation of objects using classes blueprints
// constructor is a special method of a class for creating/initializing an obeject for that class
// class ClassName {
// 	constructor (objectValue, objectValueB, ...) {
// 		this.objectPropertyA = objectValueA;
// 		this.objectPropertyB = objectValueB;
// 	}
// }

class Car {
	constructor(brand, name, year) {
		this.carBrand = brand;
		this.carName  = name;
		this.carYear  = year;
	}
}

let car1 = new Car('Mistubishi', 'Lancer', '2020');
console.log(car1);

car1.carBrand = 'Nissan';
console.log(car1);